extern printf
extern scanf

section .data
	abc: db "Enter your name: ",10,0   
	grt: db "Hi  	",0
	out: db "%s",0
	fout: db "%s%s", 0
	name: db "",0
section .text
          global main

main:
       	push ebp
       	mov ebp,esp
	
	push abc
        push out
	call printf

       	push name
	push out
	call scanf

	push name	
	push grt
	push fout
	call printf	
	
	mov esp, ebp
	pop ebp
	ret
	
